# somconnexio.coop inventory

Inventory to manage the sommobilitat.coop website instances.

You must to clone the [wp-provisioning](https://git.coopdevs.org/codeccoop/wp/wp-provisioning) in the same path of this repository:

```
.
├── wp-provisioning
└── wp-sc-inventory
```

## Setup

```sh
cd wp-provisioning
pyenv virtualenv 3.10.7 wp-provisioning
pyenv exec pip install -r requirements.txt
pyenv exec ansible-galaxy install -r requirements.yml
```

## Development

## Local

- Install [devenv](https://github.com/coopdevs/devenv)
- Run the following

```sh
cd wp-sm-inventory
devenv
cd ../wp-provisioning
pyenv exec ansible-playbook playbooks/sys_admins.yml -u root --limit=local --ask-vault-pass -i ../wp-sc-inventory/hosts
pyenv exec ansible-playbook playbooks/provision.yml --limit=local --skip-tags=monitoring,backup --ask-vault-pass -i ../wp-sc-inventory/hosts
pyenv exec ansible-playbook playbooks/deployment.yml --limit=local --ask-vault-pass -i ../wp-sc-inventory/hosts
```

## Staging

```sh
pyenv exec ansible-playbook playbooks/sys_admins.yml --limit=staging --ask-vault-pass -i ../wp-sc-inventory/hosts
pyenv exec ansible-playbook playbooks/provision.yml --limit=staging --skip-tags=monitoring,backup --ask-vault-pass -i ../wp-sc-inventory/hosts
pyenv exec ansible-playbook playbooks/deployment.yml --limit=staging --ask-vault-pass -i ../wp-sc-inventory/hosts
```

> Password in BW: `Ansible Secret somconnexio wp vault | staging`

## Production

```sh
pyenv exec ansible-playbook playbooks/sys_admins.yml -u root --limit=production --ask-vault-pass -i ../wp-sc-inventory/hosts
pyenv exec ansible-playbook playbooks/provision.yml --limit=production --ask-vault-pass -i ../wp-sc-inventory/hosts
pyenv exec ansible-playbook playbooks/deployment.yml --limit=production --ask-vault-pass -i ../wp-sc-inventory/hosts
```

> Password in BW: `Ansible Secret sommobilitat wp vault | production`
